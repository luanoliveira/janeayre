	<footer id="rodape">
		<div class="container">

			<div class="row">
				<div class="col-sm-4">
					<div class="box">
					<a class="twitter-timeline" href="https://twitter.com/janeayresouto" data-widget-id="690885255734136832">Tweets de @janeayresouto</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div><!-- .box -->
				</div><!-- .col-sm-4 -->

				<div class="col-sm-4">
					<div class="box">
					<div class="fb-page" data-href="https://www.facebook.com/janeayrealmeidasouto" data-tabs="timeline" data-width="100%" data-height="280px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/janeayrealmeidasouto"><a href="https://www.facebook.com/janeayrealmeidasouto">Janeayre Souto</a></blockquote></div></div>
					</div><!-- .box -->
				</div><!-- .col-sm-4 -->

				<div class="col-sm-4">
					<div class="box">
						<form id="newsletter">
							<header>
								<h1>Cadastre-se em minha lista</h1>
								<h2>Receba em seu e-mail todas as novidades</h2>

								<input type="email" name="email" placeholder="Seu e-mail">

								<button type="submit" class="botao">Cadastrar</button>
							</header>
						</form>
					</div>
				</div><!-- .col-sm-4 -->
			</div><!-- .row -->

			<div class="row">
				<div class="col-sm-5">
					<p class="creditos">Todos os direitos reservados © 2016 Janeayre Souto</p>
				</div><!-- .col-sm-5 -->

				<div class="col-sm-2 col-xs-6">
					<div class="stiga">
						<a href="">
							<img src="assets/images/stiga.png" alt="Agência Stiga">
						</a>
					</div>
				</div><!-- .col-sm-2 .col-xs-6 -->

				<div class="col-sm-5 col-xs-6">
					<div class="social">
						<div class="content clearfix">
							<a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
						</div><!-- .content -->
					</div><!-- .social -->
				</div><!-- .col-sm-5 .col-xs-6 -->
			</div><!-- .row -->

		</div><!-- .container -->
	</footer><!-- #rodape -->

	<script type="text/javascript" src="assets/javascripts/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="assets/javascripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/javascripts/scripts.js"></script>
</body>
</html>