<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Janeayre Sousa</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/stylesheets/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/stylesheets/style.css">

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>

	<nav id="menuMobile">
		<ul class="clearfix">
			<li><a href="">Home</a></li>
			<li><a href="">Sobre</a></li>
			<li><a href="">Notícias</a></li>
			<li>
				<a href="">Mídia</a>
				<ul>
					<li><a href="">Vídeos</a></li>
					<li><a href="">Jornal</a></li>
				</ul>
			</li>
			<li><a href="">Contato</a></li>
		</ul>
	</nav>

	<header id="topo">
		<div class="container">
			<div class="content">

				<button id="toggle" class="hidden-md hidden-lg">
					<i class="fa fa-align-justify"></i>
				</button>

				<nav id="menu" class="hidden-xs hidden-sm">
					<ul class="clearfix">
						<li><a href="">Home</a></li>
						<li><a href="">Sobre</a></li>
						<li><a href="">Notícias</a></li>
						<li>
							<a href="">Mídia</a>
							<ul>
								<li><a href="">Vídeos</a></li>
								<li><a href="">Jornal</a></li>
							</ul>
						</li>
						<li><a href="">Contato</a></li>
					</ul>
					<div class="social">
						<div class="content clearfix">
							<a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
						</div><!-- .content -->
					</div><!-- .social -->
				</nav><!-- #menu -->

				<div class="row">
					<div class="col-md-8">
						<a href="#" id="logo">
							<img src="assets/images/logo.png">
						</a>	
					</div><!-- .col-md-8 -->

					<div class="col-md-4 hidden-xs hidden-sm">
						<a href="" target="_blank" class="propaganda">
							<img src="assets/images/tmp/propaganda.png" alt="PROPAGANDA">
						</a>
					</div><!-- .col-md-4 -->
				</div><!-- .row -->
						
			</div><!-- .content -->
		</div><!-- .container -->
	</header><!-- #topo -->