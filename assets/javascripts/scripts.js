;(function($, window) {

	$("#toggle").on("click", function( event ) {
		event.preventDefault();
		$(this).toggleClass("ativo");
		$("#menuMobile").toggleClass("ativo");
		$("body").toggleClass("noOverflow");
	});

})(jQuery, window);