<?php require 'components/topo.php'; ?>

	<h1 class="tpage">Sobre</h1>

	<div class="container">
		
		<div class="row">
			<div class="col-md-6">
				<picture class="picture mb30">
					<img src="http://www.imagenstop.blog.br/wp-content/uploads/2015/03/Imagens-Paisagens.jpg" alt="TITULO DA PHOTO">
				</picture>
			</div><!-- .col-md-6 -->

			<div class="col-md-6">
				<div class="mb30">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris neque nisl, tempor eget nisl et, porta pretium risus. Morbi vitae scelerisque dolor. Cras hendrerit non eros at porta. Morbi vitae ante ullamcorper, interdum ipsum vel, feugiat neque. Donec posuere nunc lorem, in congue lorem iaculis sed. Aenean id felis condimentum ligula tincidunt viverra. Sed vehicula tellus sed ullamcorper tempus. Aliquam nec placerat diam. Mauris condimentum ultrices libero, eget varius leo elementum mollis.</p>

					<p>Curabitur sed efficitur ex, vel tristique nunc. Sed vitae dolor purus. Sed tristique consequat eros. Sed tempor, ante nec efficitur venenatis, nulla massa efficitur sem, ac vestibulum ligula sapien sit amet neque. Duis rutrum purus mi, id dapibus ante rutrum in. Cras ac ante varius, molestie dui quis, tristique dolor. Pellentesque semper quis nulla vitae maximus. Proin quis volutpat elit. Donec nulla nunc, placerat a lobortis ac, accumsan quis augue. Maecenas erat neque, pretium vel urna ac, ullamcorper imperdiet leo. Fusce auctor leo a dolor ultrices gravida. Morbi eu dui maximus, feugiat est eu, lacinia neque. Suspendisse a eleifend leo. Nulla sodales, sem ac fringilla dictum, ante erat pellentesque elit, at maximus mi metus non enim.</p>
				</div>
			</div><!-- .col-md-6 -->
		</div><!-- .row -->

	</div><!-- .container -->

<?php require 'components/rodape.php'; ?>