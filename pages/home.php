<?php require 'components/topo.php'; ?>

	<div class="container">
		
		<div class="row">
			<div class="col-sm-6">
				<a class="destaque ultimo" href="#">

					<picture>
						<img src="http://br.cdn01.mundotkm.com/2015/06/divergente1.jpg" alt="TITULO DA NOTÍCIA">
					</picture>

					<header>
						<time>13 de agosto de 2015</time>
						<h1>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos</h1>
					</header>

				</a><!-- .destaque -->
			</div><!-- .col-sm-6 -->

			<div class="col-sm-6">

				<a class="destaque" href="#">

					<picture>
						<img src="http://us.123rf.com/450wm/Kurhan/Kurhan1310/Kurhan131000471/23182179-olho-humano-na-tecnologia-do-projeto-do-fundo-cyberspace-conceito.jpg" alt="TITULO DA NOTÍCIA">
					</picture>

					<header>
						<time>13 de agosto de 2015</time>
						<h1>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos</h1>
					</header>

				</a><!-- .destaque -->

				<a class="destaque" href="#">

					<picture>
						<img src="http://us.123rf.com/450wm/Kurhan/Kurhan1310/Kurhan131000471/23182179-olho-humano-na-tecnologia-do-projeto-do-fundo-cyberspace-conceito.jpg" alt="TITULO DA NOTÍCIA">
					</picture>

					<header>
						<time>13 de agosto de 2015</time>
						<h1>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos</h1>
					</header>

				</a><!-- .destaque -->

			</div><!-- .col-sm-6 -->
		</div><!-- .row -->

		<h1 class="titulo"><span>Notícias</span></h1>

		<div class="row">
			<?php for ($noticia=1; $noticia <= 6; $noticia++) : ?> 
			<div class="col-md-4 col-sm-6">
				<article class="archive">
					<a href="">
						<picture>
							<img src="http://2.bp.blogspot.com/-wtKWxMQuyRc/TiXZPsEPSbI/AAAAAAAAAGw/pWZO1Yscu3o/s1600/1-glandula-terceiro-olho-300x205.jpg" alt="TÍTULO DA NOTÍCIA">
						</picture>

						<header>
							<h1 class="subtitulo">Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos</h1>
						</header>
					</a>
				</article><!-- archive -->
			</div><!-- .col-md-4 .col-sm-6 -->
			<?php endfor; ?>
		</div><!-- .row -->

		<div class="paginacao mb30">
			<div class="content">
				<a href="" class="pag"><<</a>
				<a href="" class="pag"><</a>
				<a href="" class="pag">1</a>
				<a href="" class="pag">2</a>
				<a href="" class="pag">3</a>
				<a href="" class="pag current">4</a>
				<a href="" class="pag">5</a>
				<a href="" class="pag">></a>
				<a href="" class="pag">>></a>
			</div><!-- .content -->
		</div><!-- .paginacao -->

		<div class="row">
			<div class="col-md-8 col-sm-6">
				<h1 class="titulo"><span>Vídeos</span></h1>
				<iframe width="100%" height="365" src="https://www.youtube.com/embed/BZDvW7CNO8o?list=RDgzAku4z2ay0" frameborder="0" allowfullscreen></iframe>
				<h2 class="subtitulo mb30">Criolo, Emicida - A Cada Vento</h2>
			</div><!-- .col-md-8 -->

			<div class="col-md-4 col-sm-6">
				<h1 class="titulo"><span>Jornal do SINSP/RN</span></h1>
				<picture class="picture mb30">
					<img src="assets/images/tmp/jornal.png" alt="TITULO DO JORNAL">
				</picture>
			</div><!-- .col-md-4 -->
		</div><!-- .row -->

	</div><!-- .container -->

<?php require 'components/rodape.php'; ?>